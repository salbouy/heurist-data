import json
import re
import urllib.parse
from collections import OrderedDict

from const import API, DB, HEURIST, CLEANR
from utils import no_info
from utils import get_url_response, no_info


def get_url(q, link="direct_links", out="json"):
    return f"{API}?db={DB}&q={q}&linkmode={link}&format={out}&a=1&depth=all&defs=0&extended=2"


def get_type_url(rectype_id, link="direct_links"):
    q = "t%3A"
    if isinstance(rectype_id, OrderedDict):
        for i in rectype_id.values():
            q = f"{q}{i}%2C"

        q = q[:-3]
    else:
        q = f"t%3A{rectype_id}"

    return get_url(q, link)


def get_field_url(field_id, value="", link="direct_links"):
    """
    To search a value within a field
    If value == "", it will return all records for which this field is not empty
    :param field_id:
    :param value:
    :param link: string
    :return: string
    """
    if value != "":
        value = str('"' + str(value)) + '"'

    return get_url("f%3A" + str(field_id) + "%3A" + str(value) + "", link)


def get_id_url(hid, link="direct_links"):
    """
    Returns the URL that can be used to retrieve a specific record using its id
    :param hid: int
    :param link: string
    :return: string
    """
    return get_url(f"id%3A{hid}", link)


def get_ids_url(hids, link="direct_links"):
    """
    Returns the URL that can be used to retrieve a list of records using their ids
    :param hids: list
    :param link: string
    :return: string
    """
    # return get_url("id%3A" + str(hids) + "", link)


def search_records(rectype_id, field_id, field_val, link="direct_links"):
    """
    Returns the records associated with a query where a field corresponds to a field value
    :param rectype_id:
    :param field_id:
    :param field_val:
    :param link:
    :return: string
    """
    if field_val != "":
        field_val = str('"' + str(field_val)) + '"'

    url = get_url(f"t%3A{rectype_id}%20f%3A{field_id}%3A{field_val}", link)
    return get_url_response(url)


def get_record(hid, link="direct_links"):
    """
    Returns a specific record using its id
    :param hid:
    :param link:
    :return: string
    """
    return get_url_response(get_id_url(hid, link))


def get_records(url):
    return get_url_response(url)["heurist"]["records"]


# Returns a list of all the records of a certain type
def get_type_records(rectype_id, link="direct_links"):
    return get_records(get_type_url(rectype_id, link))


def set_link(dataset, main_type, link, source, locus):
    sub_type = "ms" if link["ms"] else "edition"
    if main_type != "work":
        main_type = sub_type
        sub_type = "work"

    main_id = link[main_type]["id"]
    if main_id in dataset:
        dataset[main_id] = {}

    ref = f"{link[sub_type]['id']}+{link[sub_type]['title']}"
    if ref not in dataset[main_id]:
        dataset[main_id][ref] = {}

    if source not in dataset[main_id][ref]:
        dataset[main_id][ref][source] = []

    main_id = main_id
    dataset[main_id][ref][source].append(locus + link["id"])
    return dataset


def format_rec(record, rec_map, with_title=False):
    formatted_rec = {"id": record["rec_ID"]}

    if with_title:
        formatted_rec["title"] = record["rec_Title"]

    for field in record["details"]:
        if int(field["dty_ID"]) in rec_map:
            field_name = rec_map[field["dty_ID"]]
            value = field["termLabel"] if "termLabel" in field else field["value"]
            if field_name in formatted_rec:
                if type(formatted_rec[field_name]) is list:
                    formatted_rec[field_name].append(value)
                else:
                    val = formatted_rec[field_name]
                    formatted_rec[field_name] = [val, value]
            else:
                formatted_rec[field_name] = value
    return formatted_rec


def get_field_val(record, field_ids: list):
    if field_ids is None:
        field_ids = []
        # TODO: faire en sorte d'utiliser cette technique pour tous les fields en fonction du rectype id

    field_val = {}
    for field in record["details"]:
        if field["dty_ID"] in field_ids:
            field_name = field["fieldName"]

            field_val[field_name] = field["termLabel"] if "termLabel" in field else field["value"]
    return field_val


def get_tpaq(record):
    no_date = no_info()
    tpq = record["tpq"] if record["tpq"] else no_date
    taq = record["taq"] if record["taq"] else no_date

    if tpq == taq:
        return f"{tpq}"
    else:
        if tpq == no_date or taq == no_date:
            year = record["tpq"] if record["tpq"] else record["taq"]
            return f"c. {year}"
        if type(tpq) is list:
            return get_from_to(tpq[0], tpq[1])
        return f"{tpq}-{taq}"


def get_from_to(begin, end):
    if begin == end and begin != "?":
        return f"{begin}"
    else:
        if type(begin) is list:
            return get_from_to(begin[0], end[1])

    return f"{begin}-{end}"


def format_val(val):
    title_val = []
    if val["title"]:
        title_val = val["title"]
    else:
        for v in val:
            title_val.append(v["title"])
    return "<br>".join(title_val) if type(title_val) is list else title_val


def get_edit_url(hid):
    return f"{HEURIST}/hclient/framecontent/recordEdit.php?db={DB}&recID={hid}&fmt=edit"


def get_search_url(hid):
    return f"{HEURIST}/?db={DB}&w=a&q=%5B%7B%22ids%22%3A%22{hid}%22%7D%5D"
