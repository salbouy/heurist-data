import re

DB = "albouy_ALFA"
HEURIST = "https://heurist.huma-num.fr/heurist"
API = f"{HEURIST}/hsapi/controller/record_output.php"

OUTPUT_DIR = "outputs"

# * * * * * * * * * * * * #
# * * HEURIST MAPPING * * #
# * * * * * * * * * * * * #

# Rectypes
WORK = 54
MS = 58
ED = 62
LINK = 59
LIBRARY = 57
AUTHOR = 53
AUTHORITY = 56

MS_MAP = {
    "1": "shelfmark",
    "934": "library",  # name of the library in the "title" field
    "10": "tpq",
    "11": "taq",
    "238": "place",  # name of the place in the "title" field
    "943": "title",
    "953": "editor",
    "944": "isEarlyPrinted",
    "945": "extent",
    "946": "dimension",
    "952": "collection",
    "960": "iiif",
    "963": "URL",
    "17": "scan"
}

ED_MAP = {
    "1": "title",
    "953": "editor",
    "9": "date",
    "10": "tpq",
    "11": "taq",
    "238": "place",  # name of the place in the "title" field
    "941": "author",
    "945": "extent",
    "946": "dimension",
    "960": "iiif",
    "963": "URL",
    "967": "scan"
}

WORK_MAP = {
    "1": "title",
    "949": "author",  # name of the author in the "title" field
    "930": "tpq",
    "931": "taq",
    "238": "place",  # name of the place in the "title" field
    "958": "work type",
    "961": "incipit",
    "971": "confidence"
}

LINK_MAP = {
    "935": "work",
    "936": "ms",
    "937": "from",
    "938": "to",
    "966": "edition",
    "939": "authority",  # name of the authority in the "title" field
    "955": "dishas",
    "957": "source",
    "969": "canvas"
}

WORK_CANVAS_MAP = {
    "937": "from",
    "938": "to",
    "936": "ms",
    "935": "work",
    "966": "edition"
}

CANVAS_MAP = {
    "969": "canvas",
    "936": "ms",
    "966": "edition"
}

WORK_AUTH_MAP = {
    "935": "work",
    "949": "author",
    "959": "confidence"
}

MANIFEST_MAP = {
    "960": "iiif",
    "1": "title"
}

WORK_TYPE = {
    "6257": "Table",
    "6258": "Text",
    "6263": "Canon",
    "6264": "Instrument text",
    "6265": "Theoretical",
    "6266": "Mathematical",
    "6267": "Observational",
    "6268": "Miscellaneous",
}

CONFIDENCE = {
    "6270": "certain",
    "6271": "supposed",
    "6272": "attributed"
}

SOURCE_ICON = {
    "0": "",  # no indication on the information source
    "6260": "info-source glyphicon glyphicon-eye-open",  # physical consultation
    "6261": "info-source glyphicon glyphicon-bookmark",  # catalog or bibliographic information
    "6262": "info-source glyphicon glyphicon-camera"  # digital or microfilm
}

CLEANR = re.compile('<.*?>')
