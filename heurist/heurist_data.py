import json
import urllib.parse

from basic_queries import get_records, get_url
from heurist.const import LINK, ED, MS, WORK


def get_iiif_manifests(work_hid, rec_type=58):
    """
    Retrieve all primary source work records that are linked to the work whose id is given as argument
    and linked to a manuscript/an edition that have a IIIF manifest provided
    :param work_hid:
    :param rec_type:
    :return:
    """
    link = 936 if rec_type == 58 else 966
    q = {
        {"t": "59"},
        {"linked_to:935": [{"t": "59"}, {"ids": f"{work_hid}"}]},
        {f"linked_to:{link}": [{"t": f"{rec_type}"}, {"f:960": ""}]}
    }
    return get_records(get_url(urllib.parse.urlencode(json.dumps(q)), "direct"))


def get_all_iiif_manifests(work_hid):
    """
    Retrieve all edition and manuscript records linked to the work given as parameter that contain a IIIF manifest
    :param work_hid:
    :return:
    """
    q = [
        {"t": f"{WORK}"},
        {"f:title": f"{work_hid}"},
        {"sortby": "t"}
    ]
    r = [{
        "query": f"t:{LINK} linked_to:{WORK}-935",
        "codes": [f"{WORK}", "935", "", f"{LINK}", "", 1],
        "levels": [{
            "query": f"t:{MS} linkedfrom:{LINK}-936 f:960:",
            "codes": [f"{LINK}", "936", "", f"{MS}", "f:960:", 2],
            "levels": []
        }]}, {
        "query": f"t:{LINK} linked_to:{WORK}-935 ",
        "codes": [f"{WORK}", "935", "", f"{LINK}", "", 1],
        "levels": [{
            "query": f"t:{ED} linkedfrom:{LINK}-966 f:960:",
            "codes": [f"{LINK}", "966", "", f"{ED}", "f:960:", 2],
            "levels": []
        }]}]
    q = urllib.parse.urlencode(json.dumps(q))
    r = urllib.parse.urlencode(json.dumps(r))
    return get_records(get_url(f"{q}&rules={r}&rulesonly=2&a=1", "direct"))
