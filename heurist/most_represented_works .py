from const import API, DB, LINK, LINK_MAP, MS, MS_MAP, ED, ED_MAP
from basic_queries import get_url_response, format_rec, get_field_val
from utils import trim_html, kval, write_csv

link_ms = {
    "id_ms": {
        "link_id": [0, 1]
    }
}

ms_id_title = {
    "ms_id": "ms_title"
}

link_id_title = {
    "link_id": "link_title"
}

r = get_url_response(f"{API}?db={DB}&q=t%3A{LINK}&a=1&depth=all&linkmode=none&format=json&defs=0&extended=2")

for record in r['heurist']['records']:
    idx, title = record["rec_ID"], trim_html(record["rec_Title"])
    if record['rec_RecTypeID'] == f"{LINK}":
        link_id_title[idx] = title
        link_meta = get_field_val(record, [936, 937, 938, 966])
        ms_id = link_meta["Manuscript (Primary source)"]["id"] if "Manuscript (Primary source)" in link_meta else "?"
        if ms_id not in link_ms:
            link_ms[ms_id] = {}
        link_ms[ms_id][idx] = [kval(link_meta, "Locus from"), kval(link_meta, "Locus to")]

csv = [
    ["work hid", "work title", "nb ms", "nb edition", "nb link", "link2 title"]
]
for ms_id in link_ms:
    if len(link_ms[ms_id].keys()) != 1:
        works = link_ms[ms_id]



print(csv)

write_csv(csv, "works_witnesses")