from const import API, DB, LINK, LINK_MAP, MS, MS_MAP, ED, ED_MAP, HEURIST
from basic_queries import get_url_response, format_rec, get_field_val, get_from_to
from utils import trim_html, kval, write_csv

link_ms = {
    "id_ms": {
        "link_id": [0, 1]
    }
}

ms_id_title = {
    "ms_id": "ms_title"
}

work_id_title = {
    "work_id": "work_title"
}

link_info = {
    "link_id": {
        "locus": "1r-2v",
        "work": "work_id",
        "ms": "ms_id"
    }
}

r = get_url_response(f"{API}?db={DB}&q=t%3A{LINK}&a=1&depth=all&linkmode=none&format=json&defs=0&extended=2")


def folio_into_pages(locus):
    if locus is None:
        return None
    if locus.isdigit():
        return int(locus)
    locus, side = locus[:-1], locus[-1:]
    if not locus.isdigit():
        return None
    return int(locus) * 2 + (1 if side == "v" else 0)


def is_in_range(value, boundaries):
    m, M = boundaries
    return m + 1 < value < M - 1


def unpackable(li):
    if type(li) is list:
        try:
            _, _ = li[0], li[1]
            return True
        except ValueError:
            return False
        except IndexError:
            return False
    return False


def url(ids):
    return f"{HEURIST}/?db={DB}&w=a&q=%5B%7B%22ids%22%3A%22{','.join(str(id) for id in ids)}%22%7D%5D"


def overlap(lists):
    # lists.sort(key=lambda x: x[0])
    overlapping_lists = []
    lists2 = lists.copy()
    for list in lists:
        if not unpackable(list):
            continue
        l1, l2 = list[0], list[1]
        lists2.remove(list)

        for list2 in lists2:
            if not unpackable(list2):
                continue
            l3, l4 = list2[0], list2[1]
            if l3 <= l1 <= l4 or l3 <= l2 <= l4:
                overlapping_lists.append([list, list2])
    return overlapping_lists if len(overlapping_lists) != 0 else None


for record in r['heurist']['records']:
    link_id = record["rec_ID"]
    if record['rec_RecTypeID'] == f"{LINK}":
        link_info[link_id] = {"locus": "", "work": "", "ms": ""}
        link_meta = get_field_val(record, [936, 937, 938, 935])
        ms = link_meta["Manuscript (Primary source)"] if "Manuscript (Primary source)" in link_meta else None
        if ms:
            if ms["id"] not in link_ms:
                link_ms[ms["id"]] = {}
            l1, l2 = kval(link_meta, "Locus from"), kval(link_meta, "Locus to")
            link_ms[ms["id"]][link_id] = [l1, l2]
            link_info[link_id]["locus"] = get_from_to(l1, l2)
            link_info[link_id]["ms"] = ms["id"]
            ms_id_title[ms["id"]] = trim_html(ms["title"])
        work = link_meta["Work"] if "Work" in link_meta else None
        if work:
            work_id_title[work["id"]] = trim_html(work["title"])
            link_info[link_id]["work"] = work["id"]

csv = [
    ["ms hid", "ms title", "work1", "link1 locus", "link2 locus", "work2", "links", "link1 id", "link2 id"]
]
overlaps = []
# ranges = []
for ms_id in link_ms:
    # if there is only one work in the ms, there won't be overlap
    if len(link_ms[ms_id].keys()) != 1:
        ranges = []
        links = link_ms[ms_id]
        for link_id in links:
            if links[link_id][0] is not None:
                p1, p2 = folio_into_pages(links[link_id][0]), folio_into_pages(links[link_id][1])
                if p1 is not None and p2 is not None:
                    ranges.append([int(p1), int(p2), int(link_id)])
                else:
                    print([links[link_id][0], links[link_id][1], link_id])
        ms_overlaps = overlap(ranges)
        if ms_overlaps:
            for locus_overlap in ms_overlaps:
                loc1, loc2 = str(locus_overlap[0][2]), str(locus_overlap[1][2])
                # ["ms hid", "ms title",
                # "work1", "link1 locus", "link2 locus", "work2",
                # "links", "link1 id", "link2 id"]
                work1, work2 = link_info[loc1]["work"], link_info[loc2]["work"]
                if bool(work1) and bool(work2):
                    csv.append(
                        [
                            url([ms_id]), ms_id_title[ms_id],
                            work_id_title[work1], link_info[loc1]["locus"],
                            link_info[loc2]["locus"], work_id_title[work2],
                            url([loc1, loc2]), loc1, loc2]
                    )

print(csv, len(csv))

write_csv(csv, "overlap")
