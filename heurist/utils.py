import csv
import re

import requests

from const import CLEANR, OUTPUT_DIR


def no_info():
    return "?"


def trim_html(raw_html):
    cleantext = re.sub(CLEANR, '', raw_html)
    return cleantext


def write_csv(tabular_data, filename="csv"):
    with open(f'{filename}.csv', 'w', newline='') as f:
        writer = csv.writer(f)
        writer.writerows(tabular_data)


def get_url_response(url):
    r = requests.get(url)
    return dict(r.json())


def kval(obj, key):
    if key in obj:
        return obj[key]
    return None