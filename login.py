import json
import requests

username = "USERNAME"
pwd = "PASSWORD"
database = "DB_NAME"


def login(user, pwd, database):
	payload = {
		"username":username,
		"password":pwd,
		"db": database,
	}

	r = requests.post('https://heurist.huma-num.fr/heurist/hserv/controller/usr_info.php', data = payload)

	response = r.json()
	cookies = requests.utils.dict_from_cookiejar(r.cookies)
	return cookies["heurist-sessionid"]

if __name__ == "__main__":
    # venv/bin/python login.py
	session_id = login(username, pwd, database)
	print(session_id)
